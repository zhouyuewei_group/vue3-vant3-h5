const routes = [
  {
    path: "/dome1",
    name: "dome1",
    component: '',
    meta: {
      isKeepAlive: true,
      subMsgKey: "APP_CLASSIFY_PAGE_1",
    },
  },
];

export {
  routes
} ;
